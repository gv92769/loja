import { Component, OnInit } from '@angular/core';
import { Produto } from '../shared/produto';
import { Produtos } from '../shared/produtos';
import { ActivatedRoute } from '@angular/router';
import { MediaService } from '../shared/service/media.service';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.css']
})
export class DetalheComponent implements OnInit {

  produtosJson: any[];

  produto: Produto = new Produto();

  json: Produtos = new Produtos();

  constructor(private route: ActivatedRoute, private mediaService: MediaService) {
    this.route.queryParams.subscribe(params => {
      this.produto.id = params['id'];
    });
  }

  ngOnInit(): void {
    this.produtosJson = this.json.produtos;

    this.pesquisar();

  }

  pesquisar() {
    let object: any = this.produtosJson[this.produto.id - 1];
    this.produto.nome = object.nome;
    this.produto.categoria = object.categoria;
    this.produto.imagens = object.imagens;
    this.produto.precoMedio = object.precoMedio;
    this.produto.disponivel = object.disponivel;
    this.mediaService.media(object).subscribe(resp => {
      object.nota += Math.round(resp);
    });
  }

}
