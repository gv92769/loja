export class Produto {
  id: number = 0;
  nome: string = '';
  categoria: string = '';
  nota: number = 0;
  imagens: any[] = new Array();
  precoMedio: number = 0;
  disponivel: boolean = true;
}
