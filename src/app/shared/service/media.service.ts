import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  private urlMedia = 'http://api.mathjs.org/v4'

  constructor(private httpClient: HttpClient) { }

  montarExpr(notas: any[]) {
    let expr: any = '';

    expr += 'sum(';

    for (let i = 0; i < notas.length; i++)
      expr += i == notas.length - 1 ? notas[i]['nota'] : notas[i]['nota'] + ',';

    expr += ')/' + notas.length;

    return expr;
  }

  media(produto: any) {
    let expr: any = this.montarExpr(produto.notas);
    return this.httpClient.get<number>(this.urlMedia, {
      params: new HttpParams().set('expr', expr)
    });
  }

}
