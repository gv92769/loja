import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Produto } from '../shared/produto';
import { Produtos } from '../shared/produtos';
import { MediaService } from '../shared/service/media.service';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {

  @ViewChild('disponivel') disponivel: ElementRef;

  dropdownpreco: any[];

  dropdowncat: any[];

  produtosJson: any[];

  produtos: Produto[] = new Array();

  produtosFiltro: Produto[] = new Array();

  filtroForm: FormGroup;

  json: Produtos = new Produtos();

  constructor(private mediaService: MediaService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();

    this.dropdownpreco = [
      { nome: 'todos', value: 0, header: 'Todos' },
      { nome: 'um', value: 1, header: '$' },
      { nome: 'dois', value: 2, header: '$$' },
      { nome: 'tres', value: 3, header: '$$$' },
      { nome: 'quatro', value: 4, header: '$$$$' }
    ];

    this.dropdowncat = [
      { nome: 'todos', value: 'Todos', header: 'Todos' },
      { nome: 'blusa', value: 'Blusa', header: 'Blusa' },
      { nome: 'calca', value: 'Calça', header: 'Calça' },
      { nome: 'casaco', value: 'Casaco', header: 'Casaco' },
      { nome: 'vestido', value: 'Vestido', header: 'Vestido' },
      { nome: 'body', value: 'Body', header: 'Body' },
      { nome: 'calcados', value: 'Calçados', header: 'Calçados' },
      { nome: 'meias', value: 'Meias', header: 'Meias' }
    ];

    this.produtosJson = this.json.produtos;

    this.listar();

    this.produtosFiltro = this.produtos;

  }

  createForm() {
    this.filtroForm = this.fb.group({
      'disponivel': new FormControl('false'),
      'preco': new FormControl('0'),
      'categoria': new FormControl('Todos'),
    });
  }

  clear() {
    this.createForm();
    this.produtos = this.produtosFiltro;
  }

  listar() {

    for (let produto of this.produtosJson) {
      let object: Produto = new Produto();

      object.id = produto.id;
      object.nome = produto.nome;
      object.categoria = produto.categoria;
      object.imagens = produto.imagens;
      object.precoMedio = produto.precoMedio;
      object.disponivel = produto.disponivel;

      this.mediaService.media(produto).subscribe(resp => {
        object.nota += Math.round(resp);
      });

      this.produtos.push(object);
    }

  }

  filtro() {
    this.produtos = new Array;

    for (let produto of this.produtosFiltro) {
      let mostrar: boolean = true;

      if (this.disponivel.nativeElement.value == "true")
        mostrar = true === produto.disponivel ? true : false;

      if (mostrar == true && this.filtroForm.controls['preco'].value != 0)
        mostrar = this.filtroForm.controls['preco'].value == produto.precoMedio ? true : false;

      if (mostrar == true && this.filtroForm.controls['categoria'].value != 'Todos')
        mostrar = this.filtroForm.controls['categoria'].value == produto.categoria ? true : false;

      if (mostrar == true)
        this.produtos.push(produto);
    }
  }

}
