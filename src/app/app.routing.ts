import { ResultadoComponent } from './resultado/resultado.component';
import { DetalheComponent } from './detalhe/detalhe.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const APP_ROUTES: Routes = [
  {
    path: 'looks',
    component: ResultadoComponent
  },
  {
    path: 'detalhe',
    component: DetalheComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
